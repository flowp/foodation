<!DOCTYPE html>
<html lang="en" class="no-js">
<?php require_once('head.php') ?>

<body>
    <?php
    require_once('menu.php');
    ?>

    <section id="presentation">
        <div id="img_logo_present"><img src="images/logo.png"></div>
        <h1>Foodation. Cheaper but better for the planet.</h1>
    </section>

    <section id="functions">
        <article>
            <h2>WHAT IS FOODATION</h2>
            <p>Our mission is to place the lost value back onto food as something that should be eaten and not thrown away. Through the FOODATION app we’re raising awareness of food waste by making surplus food available for collection before a store closes its breakfast, lunch or dinner service.</p>
        </article>

        <article>
            <h2>How does it work for customers ?</h2>
            <div class="p_presentation">
                <h3>FIND FOOD</h3>
                <p>Download the App and order from restaurants & other stores near you.</p>
            </div>

            <div class="p_presentation">
                <h3>COLLECT</h3>
                <p>Head off to collect your food in the designated time window.</p>
            </div>

            <div class="p_presentation">
                <h3>EAT</h3>
                <p>Show your order confirmation, grab your food and enjoy!</p>
            </div>
        </article>

        <article>
            <h2>How does it work for merchants ?</h2>
            <div class="p_presentation">
                <h3>FIND FOOD</h3>
                <p>Find food you have to sell quickly.</p>
            </div>

            <div class="p_presentation">
                <h3>POST AN AD</h3>
                <p>Post an ad on the foodation website with the price you sell the product.</p>
            </div>

            <div class="p_presentation">
                <h3>SELL</h3>
                <p>At the end of the month, you pay a percentage of what you sold.</p>
            </div>
        </article>
    </section>

    <script src="scripts/jquery-3.2.1.min.js"></script>
    <script src="scripts/main.js"></script>
</body>
</html>