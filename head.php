<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <title>Acceuil</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="Codrops" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/inscription.css">
    <script src="js/modernizr.custom.js"></script>
    <link rel="icon" type="image/png" href="images/logo.png" />
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <script src="js/modernizr.custom.js"></script>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" >

    <title>Foodation. Cheaper but better for the planet.</title>

    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
</head>