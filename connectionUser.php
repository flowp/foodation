<!DOCTYPE html>
<html lang="en" class="no-js">
<?php require_once('head.php') ?>

<body>
    <?php
    require_once('menu.php');
    ?>
    <div class="container"> 
        <div id="contact">
            <form method="post" action="traitement_login.php">
              
                <h3>Connection</h3> <br/>

                <label for="em"></label>
              
                <fieldset>
                <input type="email" name="email" id="em" placeholder="Email">
                </fieldset>

                <label for="md"></label>
                <fieldset>
                <input type="password" name="password" id="md" placeholder="password">
                </fieldset>
                <button type="submit" name="submit" class="button" ><span>Connect !</span></button><br/>

                <div id="already_member"><a href="inscription.php">Not a member yet</a></div>

            </form>
        </div>
    </div>
  
  <?php if(isset($_GET['error'])) { echo $_GET['error']; } ?>
</body>
</html>