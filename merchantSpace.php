<!DOCTYPE html>
<html lang="en">
<?php require_once('head.php') ?>

<body>
  <?php
  require_once('menu.php');
  ?>

  <section id="merchant_space">
    <div class="white_bg">
      <h1>Merchants</h1>
      <p>Foodation, This is the application that will allow to avoid throwing food in the trash. Thus, you are fighting actively against food waste.</p>

      <div>
      <h2>Come in !</h2>
      <p>It's your time, try to fight against food waste, it's better for everyone. Don't worry, it's free the first month, and after, it's low pricing for great sharing.</p>
      </div>
    </div>
  </section>

  <section id="register_connection">
    <h2>Join the Foodation adventure</h2>
    <a href="inscriptionMerchant.php">Register (it's Free !)</a>
    <a href="connectionUser.php">Connection</a>
  </section>
  
  <script src="scripts/jquery-3.2.1.min.js"></script>
  <script src="scripts/main.js"></script>
</body>
</html>