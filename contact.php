<!DOCTYPE html>
<html lang="en" class="no-js">
<?php require_once('head.php') ?>

<body>
	<?php
	require_once('menu.php');
	?>

	<section>
		<div class="container"> 
	        <div id="contact">
	            <form method="post" action="#">
	                <h3>Contact us</h3> <br/>

	                <label for="em"></label>
	              
	                <fieldset>
	                <input type="email" name="email" id="em" placeholder="Email">
	                </fieldset>

	                <label for="msg"></label>
	                <fieldset>
	                <textarea name="msg" id="msg" placeholder="Votre message"></textarea>
	                </fieldset>
	                <button type="submit" name="submit" class="button" ><span>Contact</span></button><br/>
	            </form>
	        </div>
    	</div>

    	<article style="text-align:center;">
    		<a href="https://www.facebook.com"><img src="https://facebookbrand.com/wp-content/themes/fb-branding/prj-fb-branding/assets/images/fb-art.png" style="width:20%;"></a>
    		<div>Rejoignez nous !</div>
    	</article>
	</section>
</body>
</html>