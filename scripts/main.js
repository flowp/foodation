/**
 * Created by Alex on 17/04/2017.
 */
$(document).ready(function () {
    /**
     * On effectue tout une fois le document entier chargé.
     * Si il s'agit de cacher un contenu au chargement,
     * ajouter un display none en css
     */


    /**
     * Parallax Background
     */


    function majMontgol() {
        var scrollTop = $(window).scrollTop();
        var docHeight = $(document).height();
        var winHeight = $(window).height();
        var montgolHeight = $(".montgolfiere").height();
        var scrollPercent = (scrollTop) / (docHeight - winHeight) * 100;
        var scrollPercentMongol = scrollPercent;
        var scrollPercentRounded = Math.round(scrollPercent);

        $(".montgolfiere").css({
            // height: (100-scrollPercent)+"vh"
            top: "calc(" + (scrollPercentMongol) + "% - " + ((scrollPercent / 100) * montgolHeight) + "px)"
            // marginTop: "-"+(scrollPercent*557/100)+"px"
        });
    }

    function majParallax() {
        var scrollTop = $(window).scrollTop();
        var docHeight = $(document).height();
        var winHeight = $(window).height();
        var scrollPercent = (scrollTop) / (docHeight - winHeight) * 100;

        $(".parallaxbg").animate({
            backgroundPositionY: '-' + (scrollTop * 1.5) + 'px'
        }, 0);
    }


    $(window).scroll(function (e) { // Au Scroll
        majMontgol();
        majParallax();
    });

    $(window).resize(function (e) { // Au Resize
        majMontgol();
        majParallax();
    });


    $(".montgolfiere").click(function () {
        $("html, body").animate({scrollTop: 0}, 800);
    });


    /*********************
     * MENU TEL JS
     */
    $("#menuTel").click(function () {
        $(".parentNavElt").toggle();
        $("#menuTel").toggleClass("cross");
        $("section").toggle();
        $("footer").toggle();
    });


    /**
     * Récupère la valeure d'un parametre GET de l'URL
     * @param name Paramètre GET à récupérer
     * @returns {*} valeure du paramètre
     */
    $.urlParam = function (name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results === null) {
            return null;
        }
        else {
            return results[1] || 0;
        }
    }

    var curent_page = $.urlParam('p'); // Page courante


    /********************
     * PAGE INSCRIPTION *
     ********************/

    if (curent_page === "inscription") {

        flatpickr("#insc_date", { // On met en place le datepicker
            allowInput: true,
            "locale": "fr",
            dateFormat: "d/m/Y",
            maxDate: "today"
        });


        $("#insc_submit").click(function () {

            $("#insc_error").text(""); // On reset l'erreur

            // Tentative d'inscription
            var insc_login = $("#insc_login")[0].value;
            var insc_password = $("#insc_password")[0].value;
            var insc_captcha = $("#insc_captcha")[0].value;
            g_captcha_response = $("#g-recaptcha-response").val();


            // On reset les class input errors

            $("#insc_login").removeClass("input_error");
            $("#insc_password").removeClass("input_error");
             $("#insc_captcha").removeClass("input_error");
            // Traitement des messages serveurs

            $.post("pages/ajax_inscription.php", {
                login: insc_login,
                password: insc_password,
                captcha: insc_captcha
            }, function (data) {
                if (data === "1") { // Inscription Réussi
                    $("#insc_error").slideUp();
                    $("#insc_submit").slideUp();
                    $("#forminscript").slideUp();
                    $("#insc_success").slideDown();
                } else {

                    $("#insc_error").html(data);
                    if (data.indexOf("Login") !== -1) {
                        $("#insc_login").addClass("input_error");
                    }
                    if (data.indexOf("passe") !== -1) {
                        $("#insc_password").addClass("input_error");
                    }
                    if (data.indexOf("captcha") !== -1) {
                        $("#insc_captcha").addClass("input_error");
                    }
                }
            });
        });
    }


    if (curent_page === "connexion") {
        $("#conn_submit").click(function () {

            $("#conn_error").text(""); // On reset l'erreur

            // Tentative de connexion
            var conn_login = $("#conn_login")[0].value;
            var conn_password = $("#conn_password")[0].value;


            $.post("pages/ajax_connexion.php", {
                login: conn_login,
                password: conn_password
            }, function (data) {
                console.log(data);
                if (data === "1") { // Connexion Réussi
                    document.location.href = "";
                } else {

                    $("#conn_error").html(data);

                    if (data.indexOf("Login") !== -1) {
                        $("#conn_login").addClass("input_error");
                    }
                    if (data.indexOf("Passe") !== -1) {
                        $("#conn_password").addClass("input_error");
                    }

                }
            });

        });
    }
    
    
    if (curent_page === "creer_biere") {
        $("#creer_submit").click(function () {

            $("#creer_error").text(""); // On reset l'erreur

            // Tentative de connexion
            var creer_nom = $("#creer_nom")[0].value;
            var creer_type = $("#creer_type")[0].value;
            var creer_degre = $("#creer_degre")[0].value;
            var creer_brasseur = $("#creer_brasseur")[0].value;

            $.post("pages/ajax_creerBierre.php", {
                nom: creer_nom,
                type:creer_type,
                degre:creer_degre,
                brasseur:creer_brasseur
            }, function (data) {
                if (data === "1") { // Connexion Réussi
                    document.location.href = "index.php?p=index";
                } else {

                    $("#conn_error").html(data);

                    if (data.indexOf("Nom") !== -1) {
                        $("#creer_nom").addClass("input_error");
                    }

                }
            });

        });
    }
    
    if (curent_page === "creer_avis") {
    
        $note = 1;

        $("#star1").click(function () {
            $note = 1;
            
            
            for (i=0; i<$note; i++) {
                $("#star"+(i+1)).attr({
                  src: "images/star.png"
                });
            }
            
            for (i=$note; i<5; i++) {
                $("#star"+(i+1)).attr({
                  src: "images/staroff.png"
                });
            }
            
        });
        
        $("#star2").click(function () {
            $note = 2;
            
            for (i=0; i<$note; i++) {
                $("#star"+(i+1)).attr({
                  src: "images/star.png"
                });
            }
            
            for (i=$note; i<5; i++) {
                $("#star"+(i+1)).attr({
                  src: "images/staroff.png"
                });
            }
        });
        
        $("#star3").click(function () {
            $note = 3;
            
            for (i=0; i<$note; i++) {
                $("#star"+(i+1)).attr({
                  src: "images/star.png"
                });
            }
            
            for (i=$note; i<5; i++) {
                $("#star"+(i+1)).attr({
                  src: "images/staroff.png"
                });
            }
        });
        
        $("#star4").click(function () {
            $note = 4;
            
            for (i=0; i<$note; i++) {
                $("#star"+(i+1)).attr({
                  src: "images/star.png"
                });
            }
            
            for (i=$note; i<5; i++) {
                $("#star"+(i+1)).attr({
                  src: "images/staroff.png"
                });
            }
        });
        
        $("#star5").click(function () {
            $note = 5;
            
            for (i=0; i<$note; i++) {
                $("#star"+(i+1)).attr({
                  src: "images/star.png"
                });
            }
            
            for (i=$note; i<5; i++) {
                $("#star"+(i+1)).attr({
                  src: "images/staroff.png"
                });
            }
        });
        
        
        $("#avis_submit").click(function () {

            $("#creer_error").text(""); // On reset l'erreur

            // Tentative de connexion
            var avis_note = $note;
            var avis_commentaire = $("#avis_commentaire")[0].value;
            var id_biere = $("#idbiere")[0].value;

            $.post("pages/ajax_donnerAvis.php", {
                note: avis_note,
                commentaire:avis_commentaire,
                id_biere: id_biere
            }, function (data) {
                if (data === "1") { // Connexion Réussi
                    document.location.href = "index.php?p=biere_info&id="+id_biere;
                } else {

                    $("#conn_error").html(data);

                }
                console.log(data);
            });

        });
        
    }

    $("#search_biere").keyup(function() {
        alert( "Handler for .change() called." );
    });

});