<?php
session_start();
$connect = new PDO("mysql:host=127.0.0.1;dbname=tuto;charset=utf8","root","");

if(isset($_POST['submit'])) {
	if(!empty($_POST['email']) AND !empty($_POST['password'])) {
		$email = htmlspecialchars($_POST['email']);
		$password = htmlspecialchars($_POST['password']);
		$recovery_user = $connect->prepare("SELECT * FROM users WHERE email = ? AND password = ? ");
		$recovery_user->execute(array($email,$password));
		if($recovery_user->rowcount() == 1){
			$info_user = $recovery_user->fetch();
			$_SESSION['id'] = $info_user['id'];
			$_SESSION['pseudo'] = $info_user['pseudo'];
			$_SESSION['email'] = $info_user['email'];
			$_SESSION['password'] = $info_user['password'];


			header('location:profile.php');
			
		}else{
			header('location:login.php?error=Mot de passe ou email incorrect');
		}

	}else{
		header('location:login.php?error=Veuillez remplir tous les champs');
	}

}else{
	header('location:login.php?error=Formulaire non envoyé');
}
?>